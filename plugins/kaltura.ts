import { Plugin } from '@nuxt/types'
import * as kaltura from 'kaltura-player-js'

declare module 'vue/types/vue' {
  interface Vue {
    $kaltura?: any
  }
}

const plugin: Plugin = (_, inject) => {
  inject('kaltura', kaltura)
}

export default plugin
